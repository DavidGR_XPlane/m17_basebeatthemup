using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class spawnerController : MonoBehaviour
{
    //
    [Header("Prefabs")] [SerializeField] private GameObject prefabMelee;
    [SerializeField] private GameObject prefabShoot;

    [Header("Events")] public GameEvent OnWaveChange;

    [Header("SO - Parameters")] [SerializeField]
    private List<Enemy> enemyParameters;

    // faMelee, faShoot, slMelee, slShoot
    [Header("Spawn Positions")] [SerializeField]
    private List<Transform> spawnPositions;

    private int activeEnemiesNumber;

    private int round;

    public void InitSpawn()
    {
        activeEnemiesNumber = 0;
        round = 1;
        ManageSpawn(round);
    }

    private void ManageSpawn(int round)
    {
        switch (round)
        {
            case 1:
                for (int i = 0; i < 3; i++)
                {
                    print(i);
                    GameObject enemy = Instantiate(prefabMelee);
                    activeEnemiesNumber++;
                    int rnd = Random.Range(0, 2);
                    if (rnd == 0)
                    {
                        enemy.GetComponent<enemyMeleeController>().color = enemyParameters[0].Color;
                        enemy.GetComponent<enemyMeleeController>().walkSpeed = enemyParameters[0].Speed;
                        enemy.GetComponent<enemyMeleeController>().health = enemyParameters[0].Health;
                        enemy.GetComponent<Transform>().position = spawnPositions[Random.Range(0, 3)].position;
                    }
                    else
                    {
                        enemy.GetComponent<enemyMeleeController>().color = enemyParameters[2].Color;
                        enemy.GetComponent<enemyMeleeController>().walkSpeed = enemyParameters[2].Speed;
                        enemy.GetComponent<enemyMeleeController>().health = enemyParameters[2].Health;
                        enemy.GetComponent<Transform>().position = spawnPositions[Random.Range(0, 3)].position;
                    }
                }

                break;
            case 2:
                for (int i = 0; i < 3; i++)
                {
                    print(i);
                    GameObject enemy = Instantiate(prefabShoot);
                    activeEnemiesNumber++;
                    int rnd = Random.Range(0, 2);
                    if (rnd == 0)
                    {
                        enemy.GetComponent<enemyMeleeController>().color = enemyParameters[1].Color;
                        enemy.GetComponent<enemyMeleeController>().walkSpeed = enemyParameters[1].Speed;
                        enemy.GetComponent<enemyMeleeController>().health = enemyParameters[1].Health;
                        enemy.GetComponent<Transform>().position = spawnPositions[Random.Range(0, 3)].position;
                    }
                    else
                    {
                        enemy.GetComponent<enemyMeleeController>().color = enemyParameters[3].Color;
                        enemy.GetComponent<enemyMeleeController>().walkSpeed = enemyParameters[3].Speed;
                        enemy.GetComponent<enemyMeleeController>().health = enemyParameters[3].Health;
                        enemy.GetComponent<Transform>().position = spawnPositions[Random.Range(0, 3)].position;
                    }
                }

                break;
            default:

                break;
        }
    }

    public void RemoveActiveEnemy()
    {
        activeEnemiesNumber--;

        if (activeEnemiesNumber == 0)
        {
            AdvanceRound();
        }
    }

    private void AdvanceRound()
    {
        print("Round PRE" + round);
        round++;
        print("Round POST" + round);
        OnWaveChange.Raise();
        ManageSpawn(round);
    }
}