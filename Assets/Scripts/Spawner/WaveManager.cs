using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    [Header("Wave Controller")] public WaveController waveController;
    [Header("Events")] public GameEvent changeWaveText;

    public void AdvanceWave()
    {
        waveController.ActualWave += 1;
        changeWaveText.Raise();
    }

    public void ResetWaves()
    {
        waveController.ActualWave = 1;
    }
}