using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UGU_Wavecontroller : MonoBehaviour
{

    public TextMeshProUGUI txt_Wave;
    public WaveController waveController;

    private void Start()
    {
        txt_Wave.text = "1";
    }

    public void UpdateTextWave()
    {
        txt_Wave.text = waveController.ActualWave.ToString();
    }

}
