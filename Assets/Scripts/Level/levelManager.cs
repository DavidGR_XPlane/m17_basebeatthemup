using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class levelManager : MonoBehaviour
{
    public GameEvent onGameStart;

    public void Start()
    {
        onGameStart.Raise();
    }

    public void EndGame()
    {
        SceneManager.LoadScene("GameOver");
    }
}