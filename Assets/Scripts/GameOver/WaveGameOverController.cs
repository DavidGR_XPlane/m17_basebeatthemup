using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveGameOverController : MonoBehaviour
{

    public WaveController waveController;
    public TextMeshProUGUI txt_Wave;

    private void Start()
    {
        String wave = "You survived " + waveController.ActualWave;
        switch (waveController.ActualWave)
        {
            case 1:
                txt_Wave.text = wave + " wave!";
                break;
            default:
                txt_Wave.text = wave + " waves!";
                break;
        }
    }

}
