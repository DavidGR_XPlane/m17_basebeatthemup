using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class enemyMeleeController : MonoBehaviour
{

    private enum SwitchMachineStates { NONE, IDLE, WALK, HIT1 };
    private SwitchMachineStates m_CurrentState;

    [Header("Events")]
    public GameEvent OnEnemyDeath;

    [Header("Parameters")]
    [SerializeField] public int health;
    [SerializeField] public float walkSpeed;
    [SerializeField] public Color color;

    private bool m_IsRotated;

    private Transform m_Transform;
    private Animator m_Animator;
    private Rigidbody2D m_Rigidbody;
    private SpriteRenderer m_SpriteRenderer;

    [Header("Areas")]
    [SerializeField] private enemy1TriggerController m_WalkDetectionArea;
    [SerializeField] private enemy1TriggerController m_AttackDetectionArea;

    private Transform m_PlayerTarget;

    //---------------BASE---------------//

    private void Awake()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_Transform = GetComponent<Transform>();
        m_WalkDetectionArea.OnEnter += TargetPlayer;
        m_WalkDetectionArea.OnExit += DeleteTarget;
        m_AttackDetectionArea.OnEnter += Attack;
        m_AttackDetectionArea.OnExit += StopAttack;
    }

    private void Update()
    {
        UpdateState();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerHitbox"))
        {
            int dmg = collision.gameObject.GetComponent<PlayerHitboxController>().damage;
            DecreaseHp(dmg);
        }
    }

    private void Attack(Transform target)
    {
        ChangeState(SwitchMachineStates.HIT1);
    }

    private void StopAttack(Transform target)
    {
        ChangeState(SwitchMachineStates.WALK);
    }

    private void TargetPlayer(Transform target)
    {
        print("player ha entrat en la meva detecci�");
        m_PlayerTarget = target;
        ChangeState(SwitchMachineStates.WALK);
    }

    private void DeleteTarget(Transform target)
    {
        print("player ha sortit de la meva detecci�");
        ChangeState(SwitchMachineStates.IDLE);
        m_PlayerTarget = null;
    }

    public void Walk()
    {
        if (!m_PlayerTarget) return;
        Vector2 target = m_PlayerTarget.position;
        Vector2 me = m_Transform.position;
        m_Rigidbody.velocity = (target - me).normalized * walkSpeed;

        ChangeState(SwitchMachineStates.WALK);
        if (m_Rigidbody.velocity.x < 0 && !m_IsRotated)
        {
            this.GetComponent<Transform>().Rotate(0, 180, 0);
            m_IsRotated = true;
            return;
        }

        if (m_Rigidbody.velocity.x > 0 && m_IsRotated)
        {
            this.GetComponent<Transform>().Rotate(0, -180, 0);
            m_IsRotated = false;
        }
    }

    public void DecreaseHp(int hp)
    {
        StartCoroutine(ChangeSpriteColor());
        health -= hp;
        showHp();
        if (health <= 0)
        {
            Destroy(gameObject);
            print("EVENTO");
            OnEnemyDeath.Raise();
        }
    }

    private void showHp()
    {
        print("Soc un enemic amb vida: " + health);
    }

    IEnumerator ChangeSpriteColor()
    {
        m_SpriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.3f);
        m_SpriteRenderer.color = Color.white;
    }

    //---------STATE MACHINE-----------//

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Animator.Play("Idle");
                break;
            case SwitchMachineStates.WALK:
                m_Animator.Play("Walk");
                break;
            case SwitchMachineStates.HIT1:
                m_Animator.Play("Hit1");
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.HIT1:

                break;

            default:

                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:
                Walk();
                break;

            case SwitchMachineStates.HIT1:

                break;

            default:

                break;
        }
    }
}
