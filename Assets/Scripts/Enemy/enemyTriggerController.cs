using System;
using UnityEngine;

public class enemy1TriggerController : MonoBehaviour
{

    public event Action<Transform> OnEnter;
    public event Action<Transform> OnExit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            OnEnter?.Invoke(collision.gameObject.transform);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            OnExit?.Invoke(collision.gameObject.transform);
        }
    }
}
