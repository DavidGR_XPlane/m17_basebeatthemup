using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyShootController : MonoBehaviour
{
    private enum SwitchMachineStates
    {
        NONE,
        IDLE,
        PATROL,
        WALK,
        SHOOT
    };

    private SwitchMachineStates m_CurrentState;

    [Header("Events")] public GameEvent OnEnemyDeath;

    [Header("Parameters")] [SerializeField]
    public int health;

    [SerializeField] public float walkSpeed;
    [SerializeField] public Color color;

    [Header("Projectile Prefab")] public GameObject projectilePrefab;

    [Header("Waypoints")] [SerializeField] private List<Transform> m_Waypoints;
    private Transform target;
    private int index;

    private Transform patrolTarget;

    private bool m_IsRotated;

    private Transform m_Transform;
    private Animator m_Animator;
    private Rigidbody2D m_Rigidbody;
    private SpriteRenderer m_SpriteRenderer;

    [Header("Areas")] [SerializeField] private enemy1TriggerController m_WalkDetectionArea;
    [SerializeField] private enemy1TriggerController m_AttackDetectionArea;

    private Vector2 m_PlayerTarget;

    //---------------BASE---------------//

    private void Awake()
    {
        index = 0;
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_Transform = GetComponent<Transform>();
        m_WalkDetectionArea.OnEnter += TargetPlayer;
        m_WalkDetectionArea.OnExit += DeleteTarget;
        m_AttackDetectionArea.OnEnter += Shoot;
        m_AttackDetectionArea.OnExit += StopShoot;
    }

    private void Start()
    {
        ChangeState(SwitchMachineStates.PATROL);
    }

    private void Update()
    {
        UpdateState();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("PlayerHitbox")) return;
        print("enemy shoot collision");
        int dmg = collision.gameObject.GetComponent<PlayerHitboxController>().damage;
        DecreaseHp(dmg);
    }

    private void Patrol()
    {
        if (Mathf.Abs(m_Waypoints[index].position.x - m_Transform.position.x) < .1f)
        {
            index = (index + 1) % m_Waypoints.Count;
            Debug.Log($"Destination point : {m_Waypoints[index].position}");
        }

        Walk(m_Waypoints[index].position);
    }

    private void Shoot(Transform target)
    {
        Turn(target.position);
        m_Rigidbody.velocity = new Vector2(0, 0);
        ChangeState(SwitchMachineStates.SHOOT);
    }

    private void ShootBullet()
    {
        GameObject bullet = Instantiate(projectilePrefab);
        bullet.transform.position = transform.position;
        bullet.transform.rotation = transform.rotation;
        bullet.GetComponent<Rigidbody2D>().AddForce(m_PlayerTarget * 3, ForceMode2D.Impulse);
    }

    private void StopShoot(Transform target)
    {
        ChangeState(SwitchMachineStates.WALK);
    }

    private void TargetPlayer(Transform target)
    {
        print("player ha entrat en la meva detecció");
        m_PlayerTarget = target.position;
        ChangeState(SwitchMachineStates.WALK);
    }

    private void DeleteTarget(Transform target)
    {
        print("player ha sortit de la meva detecció, patrullo");
        ChangeState(SwitchMachineStates.PATROL);
        m_PlayerTarget = new Vector2(0, 0);
    }

    private void Walk(Vector2 target)
    {
        // Vector2 me = m_Transform.position;
        // m_Rigidbody.velocity = (target - me).normalized * walkSpeed;
        if (target.x > transform.position.x)
            m_Rigidbody.velocity = Vector3.right * walkSpeed;
        else if (target.x < transform.position.x)
            m_Rigidbody.velocity = Vector3.left * walkSpeed;
        Turn(target);
    }

    private void Turn(Vector2 target)
    {
        if (target.x > transform.position.x)
            m_Transform.eulerAngles = Vector3.zero;
        else if (target.x < transform.position.x)
            m_Transform.eulerAngles = Vector3.up * 180f;
    }

    public void DecreaseHp(int hp)
    {
        StartCoroutine(ChangeSpriteColor());
        health -= hp;
        showHp();
        if (health <= 0)
        {
            Destroy(gameObject);
            print("EVENTO");
            OnEnemyDeath.Raise();
        }
    }

    private void showHp()
    {
        print("Soc un enemic amb vida: " + health);
    }

    IEnumerator ChangeSpriteColor()
    {
        m_SpriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.3f);
        m_SpriteRenderer.color = Color.white;
    }

    //---------STATE MACHINE-----------//

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Animator.Play("Idle");
                break;
            case SwitchMachineStates.PATROL:
                index = 0;
                Debug.Log($"Destination: {m_Waypoints[index].position}");
                m_Animator.Play("Walk");
                break;
            case SwitchMachineStates.WALK:
                m_Animator.Play("Walk");

                break;
            case SwitchMachineStates.SHOOT:
                m_Animator.Play("Shoot");
                break;
            default:

                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.PATROL:

                break;

            case SwitchMachineStates.SHOOT:

                break;

            default:

                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:
                Walk(m_PlayerTarget);
                break;

            case SwitchMachineStates.PATROL:
                Patrol();
                break;

            case SwitchMachineStates.SHOOT:

                break;

            default:

                break;
        }
    }
}