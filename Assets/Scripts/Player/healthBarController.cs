using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthBarController : MonoBehaviour
{

    [SerializeField] private PlayerController playerController;

    private void Awake()
    {
        playerController.OnEnemyHit += DecreaseHealthBar;
    }

    public void DecreaseHealthBar(int hpToDecrease)
    {
        print("ENTRO");
        GetComponent<Image>().fillAmount -= hpToDecrease;
    }

}
