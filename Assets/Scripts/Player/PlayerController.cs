using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private enum SwitchMachineStates
    {
        NONE,
        IDLE,
        WALK,
        JUMPING,
        HIT1,
        HIT2
    };

    private SwitchMachineStates m_CurrentState;

    [Header("Input")] [SerializeField] private InputActionAsset p_InputAsset;
    private InputActionAsset p_Input;
    private InputAction m_MovementAction;

    [Header("Events")] public GameEvent OnPlayerDeath;

    //Events
    public event Action<int> OnEnemyHit;

    [Header("Parameters")] public int m_walkSpeed;
    public int m_jumpForce;
    public int m_health;

    private bool m_ComboAvailable;
    private bool m_Rotated;

    private Animator m_Animator;
    private Rigidbody2D m_Rigidbody;
    private SpriteRenderer m_SpriteRenderer;

    //---------------BASE---------------//

    private void Awake()
    {
        Assert.IsNotNull(p_InputAsset);
        p_Input = Instantiate(p_InputAsset);
        m_MovementAction = p_Input.FindActionMap("Standard").FindAction("Move");
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        StartPlayer();
        InitState(SwitchMachineStates.IDLE);
    }

    private void Update()
    {
        UpdateState();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("EnemyHitbox") || collision.CompareTag("BulletCollider"))
        {
            TakeDamage(collision);
        }
    }

    private void StartPlayer()
    {
        SubscribeAllInput();
    }

    private void TakeDamage(Collider2D collision)
    {
        int dmg = 0;
        print("me pegan");
        StartCoroutine(ChangeSpriteColor());
        if (collision.CompareTag("EnemyHitbox"))
        {
            dmg += collision.gameObject.GetComponent<enemyHitboxController>().damage;
        }
        else if (collision.CompareTag("BulletCollider"))
        {
            dmg += collision.gameObject.GetComponent<bulletController>().damage;
        }

        DecreaseHp(dmg);
    }


    private void Attack(InputAction.CallbackContext context)
    {
        print("estic atacant!!");
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                ChangeState(SwitchMachineStates.HIT1);

                break;

            case SwitchMachineStates.WALK:
                ChangeState(SwitchMachineStates.HIT1);

                break;

            case SwitchMachineStates.HIT1:
                if (m_ComboAvailable)
                {
                    ChangeState(SwitchMachineStates.HIT2);
                }
                else
                {
                    ChangeState(SwitchMachineStates.HIT1);
                }

                break;

            case SwitchMachineStates.HIT2:
                break;

            default:
                break;
        }
    }

    private void Jump(InputAction.CallbackContext context)
    {
        print("jump");
        m_Rigidbody.AddForce(Vector2.up * m_jumpForce, ForceMode2D.Impulse);
    }

    private void Move(InputAction.CallbackContext context)
    {
        m_Rigidbody.velocity = new Vector2(m_MovementAction.ReadValue<Vector2>().x, m_Rigidbody.velocity.y);
    }

    private void Die()
    {
        OnPlayerDeath.Raise();
    }

    private void DecreaseHp(int hp)
    {
        if (m_health - hp < 0)
        {
            Die();
            return;
        }

        m_health -= hp;
        OnEnemyHit?.Invoke(hp);
    }

    IEnumerator ChangeSpriteColor()
    {
        m_SpriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.3f);
        m_SpriteRenderer.color = Color.white;
    }

    private void SubscribeAllInput()
    {
        p_Input.FindActionMap("Standard").FindAction("Move").started += this.Move;
        p_Input.FindActionMap("Standard").FindAction("Jump").started += this.Jump;
        p_Input.FindActionMap("Standard").FindAction("Attack").started += this.Attack;
        p_Input.FindActionMap("Standard").Enable();
    }

    public void UnsubscribeAllInput()
    {
        p_Input.FindActionMap("Standard").FindAction("Move").started -= this.Move;
        p_Input.FindActionMap("Standard").FindAction("Jump").started -= this.Jump;
        p_Input.FindActionMap("Standard").FindAction("Attack").started -= this.Attack;
        p_Input.FindActionMap("Standard").Disable();
        //p_Input.FindActionMap("Standard").FindAction("Move").ReadValue<Vector2>().x;
    }

    //------------COMBO-----------------//

    public void EndHit()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    public void InitCombo()
    {
        m_ComboAvailable = true;
    }

    public void EndCombo()
    {
        m_ComboAvailable = false;
    }

    //-------STATE MACHINE-----------------//

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;
        ExitState();
        InitState(newState);
    }

    //
    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = new Vector2(0, m_Rigidbody.velocity.y);
                m_Animator.Play("Idle");
                break;

            case SwitchMachineStates.WALK:
                m_Animator.Play("Walk");
                break;

            case SwitchMachineStates.HIT1:
                print("hit");

                m_Animator.Play("Hit1");
                break;

            case SwitchMachineStates.HIT2:
                print("hit2");

                m_Animator.Play("Hit2");
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.HIT1:

                break;

            case SwitchMachineStates.HIT2:

                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                if (m_MovementAction.ReadValue<Vector2>().x != 0)
                {
                    ChangeState(SwitchMachineStates.WALK);
                    if (m_MovementAction.ReadValue<Vector2>().x < 0)
                    {
                        this.GetComponent<Transform>().Rotate(0, -180, 0);
                        m_Rotated = true;
                    }
                    else
                    {
                        if (m_Rotated)
                        {
                            this.GetComponent<Transform>().Rotate(0, 180, 0);
                            m_Rotated = false;
                        }
                    }
                }

                break;

            case SwitchMachineStates.WALK:
                m_Rigidbody.velocity = new Vector2(m_MovementAction.ReadValue<Vector2>().x * m_walkSpeed,
                    m_Rigidbody.velocity.y);
                if (m_Rigidbody.velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);
                break;

            case SwitchMachineStates.HIT1:

                break;

            case SwitchMachineStates.HIT2:

                break;

            default:

                break;
        }
    }
}