using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu]
public class Enemy : ScriptableObject
{

    public float Speed;
    public int Health;
    public Color Color;

}

