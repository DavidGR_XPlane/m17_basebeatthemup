using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu]
public class WaveController : ScriptableObject
{

    public int ActualWave;

}
