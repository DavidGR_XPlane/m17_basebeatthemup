using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{

    public GameEvent OnGameRetry;

    public void Retry()
    {
        OnGameRetry.Raise();
        SceneManager.LoadScene("Main");
    }

}
